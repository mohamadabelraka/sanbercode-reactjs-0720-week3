import React from 'react';
import '../App.css';
import ShowName from './ShowName.js'
import ShowPrice from './ShowPrice.js'
import ShowWeight from './ShowWeight.js'

let dataHargaBuah = [
  {nama: "Semangka", harga: 10000, berat: 1000},
  {nama: "Anggur", harga: 40000, berat: 500},
  {nama: "Strawberry", harga: 30000, berat: 400},
  {nama: "Jeruk", harga: 30000, berat: 1000},
  {nama: "Mangga", harga: 30000, berat: 500}
]
class FruitInfo extends React.Component {
  render() {
    return (
      <>
      <div class = "tabelBuah">
        <h1>Tabel Harga Buah</h1>
        <table id="tableBuah">
          <tr>
            <th name = "buah_th">Nama</th>
            <th name = "buah_th">Harga</th>
            <th name = "buah_th">Berat</th>
          </tr>
          {dataHargaBuah.map(tab=> {
            return (
              <tr>
                <td name = "buah_td"><ShowName nama={tab.nama}/></td>
                <td name = "buah_td"><ShowPrice harga={tab.harga}/></td>
                <td name = "buah_td"><ShowWeight berat={tab.berat}/></td>
              </tr>
            )
          })}
        </table>
      </div>
      </>
    )
  }
}

export default FruitInfo;
