import React, {Component} from 'react'
import '../App.css';

class InfoTimer extends Component{
  constructor(props){
    super(props)
    this.state = {
      time: 120,
      date: new Date()
    }
  }

  componentDidMount(){
    if (this.props.start !== undefined){
      this.setState({time: this.props.start})
    }
    if (this.state.time > 100) {
      this.timerID = setInterval(
        () => this.tick(),
        1000
      );
    }

  }

  componentWillUnmount(){
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      time: this.state.time - 1,
      date: new Date()
    });
  }


  render(){
    return(
      <>
        {this.state.time >= 0 &&
          <table name = "tableTimer">
            <tr>
              <td><h3>Sekarang jam : {this.state.date.toLocaleTimeString()}</h3></td>
              <td><h3>Hitung mundur : {this.state.time}</h3></td>
            </tr>
          </table>
        }
      </>
    )
  }
}

export default InfoTimer
