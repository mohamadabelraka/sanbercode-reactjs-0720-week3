import React from 'react';
import '../App.css';
import ShowName from './ShowName.js'
import ShowPrice from './ShowPrice.js'
import ShowWeight from './ShowWeight.js'


class ListFruit extends React.Component {
  constructor(props){
    super(props)
    this.state ={
      dataHargaBuah : [
        {nama: "Semangka", harga: 10000, berat: 1000},
        {nama: "Anggur", harga: 40000, berat: 500},
        {nama: "Strawberry", harga: 30000, berat: 400},
        {nama: "Jeruk", harga: 30000, berat: 1000},
        {nama: "Mangga", harga: 30000, berat: 500}
      ],
      inputName :"",
      inputPrice: "",
      inputWeight: "",
      indexOfForm: -1
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);

  }
  handleChange(event){
    this.setState({[event.target.name]: event.target.value})
  }
  handleSubmit(event){
    event.preventDefault()
    let name = this.state.inputName
    let price = this.state.inputPrice
    let weight = this.state.inputWeight

    if (name.replace(/\s/g,'') !== "" && price.toString().replace(/\s/g,'') !== "" && weight.toString().replace(/\s/g,'') !== ""){
      let newDataBuah = this.state.dataHargaBuah
      let index = this.state.indexOfForm

      if (index === -1){
        newDataBuah = [...newDataBuah, {nama: name, harga: price, berat: weight}]
      }else{
        newDataBuah[index] = {nama: name, harga: price, berat: weight}
      }

      this.setState({
        dataHargaBuah: newDataBuah,
        inputName: "",
        inputPrice: "",
        inputWeight: ""
      })
    }
  }

  handleEdit(event){
    let index = event.target.value
    let buah = this.state.dataHargaBuah[index]
    this.setState({inputName: buah.nama, inputPrice: buah.harga, inputWeight: buah.berat, indexOfForm: index})

  }


  handleDelete(event){
    let index = event.target.value
    let newBuah = this.state.dataHargaBuah
    let editedBuah = newBuah[this.state.indexOfForm]
    newBuah.splice(index, 1)

    if (editedBuah !== undefined){

      var newIndex = newBuah.findIndex((item) => item === editedBuah)
      this.setState({dataHargaBuah: newBuah, indexOfForm: newIndex})

    }else{

      this.setState({dataHargaBuah: newBuah})
    }

  }

  render() {
    return (
      <>
      <div className = "tabelBuah">
        <h1>Tabel Harga Buah</h1>
        <table id="tableBuah">
          <thead>
            <tr>
              <th name = "buah_th">No</th>
              <th name = "buah_th">Nama</th>
              <th name = "buah_th">Harga</th>
              <th name = "buah_th">Berat</th>
              <th name = "buah_th">Action</th>
            </tr>
          </thead>
          <tbody>
          {this.state.dataHargaBuah.map((tab,index)=> {
            return (
              <tr key={index}>
                <td name = "buah_td">{index+1}</td>
                <td name = "buah_td"><ShowName nama={tab.nama}/></td>
                <td name = "buah_td"><ShowPrice harga={tab.harga}/></td>
                <td name = "buah_td"><ShowWeight berat={tab.berat}/></td>
                <td name = "buah_td">
                  <button name = "buttonEdit" onClick={this.handleEdit} value={index}>Edit</button>
                  <button name = "buttonDelete" onClick={this.handleDelete} value={index}>Delete</button>
                </td>
              </tr>
            )
          })}
          </tbody>
        </table>
      </div>
      <div className = "formBuah">
        <h1>Form Data Buah</h1>
        <form id = "dataBuah" onSubmit={this.handleSubmit}>
          <label name="labelBuah">
            Masukkan nama buah:
          </label>
          <input name="inputName" type="text" value={this.state.inputName} onChange={this.handleChange}/><br/>
          <label name="labelBuah">
            Masukkan harga buah:
          </label>
          <input name="inputPrice" type="text" value={this.state.inputPrice} onChange={this.handleChange}/><br/>
          <label name="labelBuah">
            Masukkan berat buah (Dalam satuan gram):
          </label>
          <input name="inputWeight" type="text" value={this.state.inputWeight} onChange={this.handleChange}/><br/>
          <button name = "submitBuah">submit</button>
        </form>
      </div>
      </>
    )
  }
}
export default ListFruit;
