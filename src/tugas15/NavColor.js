import React from "react"
import {ColorNavProvider} from "./ColorNavContext"
import NavColorList from "./NavColorList"

const Fruit = () =>{
  return(
    <ColorNavProvider>
      <NavColorList/>
    </ColorNavProvider>
  )
}

export default Fruit
