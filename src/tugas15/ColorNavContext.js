import React, { useState, createContext } from "react";

export const ColorNavContext = createContext();

export const ColorNavProvider = props => {
  const [color, setColor] =  useState(["#e8884d", "#5bdb3b", "#d9419c", "#41b8d9", "#776cf4"])

  return (
    <ColorNavContext.Provider value={[color, setColor]}>
      {props.children}
    </ColorNavContext.Provider>
  );
}
