import React from "react"
import {ListProvider} from "./ListContext"
import FruitList from "./FruitList"
import FruitForm from "./FruitForm"

const Lists = () =>{
  return(
    <ListProvider>
      <FruitList/>
      <FruitForm/>
    </ListProvider>
  )
}

export default Lists
