import React, {useContext} from "react"
import {Link} from "react-router-dom";
import {ColorNavContext} from "./ColorNavContext";

const NavColorList = () =>{
  const [color, setColor] = useContext(ColorNavContext);

  return(
    <>
      <nav>
        <ul style={{textAlign:"center"}}>
                <li style={{
                    background:color[0],
                  fontweight: "bold",
                  display:"inline-block",
                  padding: "10px"}}>
                  <Link to="/">Home</Link>
                </li >
                <li style={{
                    background:color[1],
                  fontweight: "bold",
                  display:"inline-block",
                  padding: "10px"}}>
                  <Link to="/timer">Timer</Link>
                </li>
                <li style={{
                    background:color[2],
                  fontweight: "bold",
                  display:"inline-block",
                  padding: "10px"}}>
                  <Link to="/lists">Lists dengan class</Link>
                </li>
                <li style={{
                    background:color[3],
                  fontweight: "bold",
                  display:"inline-block",
                  padding: "10px"}}>
                  <Link to="/hook-lists">Lists dengan Hooks</Link>
                </li>
                <li style={{
                    background:color[4],
                  fontweight: "bold",
                  display:"inline-block",
                  padding: "10px"}}>
                  <Link to="/context-lists">Lists dengan Context</Link>
                </li>
        </ul>
      </nav>
    </>
  )

}

export default NavColorList
