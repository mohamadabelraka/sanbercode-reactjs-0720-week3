import React, {useContext} from "react"
import {ListContext} from "./ListContext"
import axios from "axios"

const FruitList = () =>{
  const [
  dataHargaBuah, setDataHargaBuah,
  inputName, setInputName,
  inputPrice, setInputPrice,
  inputWeight, setInputWeight,
  selectedId, setSelectedId,
  statusForm, setStatusForm] = useContext(ListContext)

  const handleDelete = (event) => {
    let idBuah = parseInt(event.target.value)

    let newDataHargaBuah = dataHargaBuah.filter(item => item.id !== idBuah)
    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
      .then(res=>{
        console.log(res);
      })

      setDataHargaBuah([...newDataHargaBuah])
  }
  const handleEdit = (event) =>{
    let idBuah = parseInt(event.target.value)
    let buah = dataHargaBuah.find(x=> x.id === idBuah)
    setInputName(buah.nama)
    setInputPrice(buah.harga)
    setInputWeight(buah.berat)
    setSelectedId(idBuah)
    setStatusForm("edit")
  }
  return(
    <>
    <div className = "tabelBuah">
      <h1>Tabel Harga Buah</h1>
      <table id="tableBuah">
        <thead>
          <tr>
            <th name = "buah_th">No</th>
            <th name = "buah_th">Nama</th>
            <th name = "buah_th">Harga</th>
            <th name = "buah_th">Berat</th>
            <th name = "buah_th">Action</th>
          </tr>
        </thead>
        <tbody>
        {dataHargaBuah !== null && dataHargaBuah.map((tab,index)=> {
          return (
            <tr key={index}>
              <td name = "buah_td">{index+1}</td>
              <td name = "buah_td">{tab.nama}</td>
              <td name = "buah_td">{tab.harga}</td>
              <td name = "buah_td">{tab.berat/1000} kg</td>
              <td name = "buah_td">
                <button name = "buttonEdit" onClick={handleEdit} value={tab.id}>Edit</button>
                <button name = "buttonDelete" onClick={handleDelete} value={tab.id}>Delete</button>
              </td>
            </tr>
          )
        })}
        </tbody>
      </table>
    </div>
    </>
  )

}

export default FruitList
