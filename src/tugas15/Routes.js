import React from "react";
import { Switch, Link, Route } from "react-router-dom";

import FruitInfo from '../tugas11/FruitInfo';
import Timer from '../tugas12/InfoTimer';
import Lists from '../tugas13/ListFruit';
import HooksLists from '../tugas14/ListFruitAxio';
import ContextLists from '../tugas15/Lists';

const Routes = () => {

  return (
    <>
      <Switch>
        <Route path="/timer" component={Timer}/>
        <Route path="/lists">
          <Lists />
        </Route>
        <Route path="/hook-lists">
          <HooksLists />
        </Route>
        <Route path="/context-lists">
          <ContextLists />
        </Route>
        <Route path="/">
          <FruitInfo />
        </Route>
      </Switch>
    </>
  );
};

export default Routes;
